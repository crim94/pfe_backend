const express = require('express');
require('dotenv').config();
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const passport = require('passport');
const morgan=require('morgan');
require('./config/passport')(passport);
var Agendash = require('agendash');
var MongoClient = require('mongodb').MongoClient;
var cors = require('cors');

var agenda = require("./jobs/agenda");

/*****************models****************************************************************/
const mongodb = require('./providers/mongodb');
const User=require('./models/User.model');
const Admin=require('./models/Admin.model');
const Categorie=require('./models/Categorie.model');
const Joueur=require('./models/Joueur.model');
const Question=require('./models/Question.model');
const Session=require('./models/Session.model');
const Participation=require('./models/Participation.model');
/****************controllers***************************************************************/
const authController=require('./controllers/authcontroller');
const adminController=require('./controllers/adminController');
const categorieController=require("./controllers/categorieController");
const questionController=require("./controllers/questionController");
const sessionController=require("./controllers/sessionController");
const joueurController=require("./controllers/joueurController");
const statisticController=require("./controllers/statisticController");
/******************************************************************************************/
let app=express();

/********************************middelware***********************************************/
mongodb.check();
app.use(cors());
app.use(fileUpload());
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('dev'));
//app.use(express.static(__dirname+"/public"));
app.use('/auth',authController);
app.use('/admin',adminController);
app.use('/categorie',categorieController);
app.use('/question',questionController);
app.use('/session',sessionController);
app.use('/joueur',joueurController);
app.use('/statistic',statisticController);

app.use('/dash', Agendash(agenda));
/*****************************************************************************************/
app.get('/home',function(req,res,next){

res.send("hello");
});
app.listen(process.env.SERVER_PORT, () => {
    console.log('*******************************************************************************')
    console.log('****************Server running at ' + process.env.SERVER_PORT + '**************')
    console.log('*******************************************************************************')
})
