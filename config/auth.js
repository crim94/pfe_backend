module.exports = {
    'facebookAuth' : {
        'clientID'      : process.env.FACEBOOK_APP_ID, // your App ID
        'clientSecret'  : process.env.FACEBOOK_APP_SECRET, // your App Secret
        'callbackURL'   : 'http://localhost:6060/auth/facebook/callback',
        'profileFields' : ['id', 'emails', 'gender', 'link', 'locale', 'name', 'timezone', 'updated_time', 'verified'] // For requesting permissions from Facebook API
    }
};
