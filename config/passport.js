// config/passport.js
var LocalStrategy    = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var User       = require('../models/User.model');
var configAuth = require('./auth');
var FacebookTokenStrategy = require('passport-facebook-token');
module.exports = function(passport) {
    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });
    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
    /***LocalStrategy********************************************/
    passport.use(new LocalStrategy({
      usernameField: 'email',
    passwordField: 'password'
  },
      function(username, password, done) {
        User.findOne({ email: username }, function (err, user) {
          if(!user.pswd) {return done(err);}
          if (err) { return done(err); }
          if (!user) { return done(null, false); }
          if (!User.verifyPassword(password,user.pswd)) { return done(null, false); }
          return done(null, user);
        });
      }
    ));
    /*************************fin LocalStrategy ***********************************/
    /*****************************passport-facebook-token Strategy ****************/
    passport.use(new FacebookTokenStrategy({
      clientID        : configAuth.facebookAuth.clientID,
      clientSecret    : configAuth.facebookAuth.clientSecret
      }, function(accessToken, refreshToken, profile, done) {
        User.findOne({'facebook.id': profile.id}, function (err, user) {
          if (err)
              return done(err);
          if (user) {
              return done(null, user); // user found, return that user
          } else {
              var newUser= new User();
              newUser.facebook.id    = profile.id; // set the users facebook id
              newUser.facebook.token = accessToken; // we will save the token that facebook provides to the user
              newUser.facebook.name  = profile.displayName; // look at the passport user profile to see how names are returned
              newUser.facebook.email=newUser.email = profile.emails[0].value|| null;
              newUser.save(function(err) {
                  if (err)
                      throw err;
                  return done(null, newUser);
              });

      }
    }
  )}));
    /**********************************end*************************************/
    passport.use(new FacebookStrategy({

        // pull in our app id and secret from our auth.js file
        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL

    },

    // facebook will send back the token and profile
    function(token, refreshToken, profile, done) {
        // asynchronous
        process.nextTick(function() {

            // find the user in the database based on their facebook id
            User.findOne({ 'facebook.id' : profile.id }, function(err, user) {

                // if there is an error, stop everything and return that
                // ie an error connecting to the database
                if (err)
                    return done(err);

                // if the user is found, then log them in
                if (user) {
                    return done(null, user); // user found, return that user
                } else {
                    // if there is no user found with that facebook id, create them
                    var newUser= new User();

                    // set all of the facebook information in our user model
                    newUser.facebook.id    = profile.id; // set the users facebook id
                    newUser.facebook.token = token; // we will save the token that facebook provides to the user
                    newUser.facebook.name  = profile.displayName; // look at the passport user profile to see how names are returned
                    //newUser.facebook.email = profile.emails[0].value|| null; // facebook can return multiple emails so we'll take the first

                    // save our user to the database
                    newUser.save(function(err) {
                        if (err)
                            throw err;

                        // if successful, return the new user
                        //const token = jwt.sign(newUser,process.env.SECRET);

                        return done(null, newUser);
                    });
                }

            });
        });

    }));

};
