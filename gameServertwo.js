const express =require('express');
const socketIO= require('socket.io');
const minimst=require('minimist');
const http = require('http');
const url =require("url");
/** connection to Redis */
const Redis = require('ioredis');

/** end connection */
require('dotenv').config();
/** mongodb connection && model imports*/
const mongodb = require('./providers/mongodb');
const redis = new Redis(process.env.REDIS_HOST_PORT,process.env.REDIS_HOST_IP);
const Question=require('./models/Question.model');
const Session=require("./models/Session.model");
const Joueur = require("./models/Joueur.model");
const Participation=require("./models/Participation.model");

mongodb.check();
/** end mongodb connection */
let app = express();
let port = 4070;
let server = http.createServer(app).listen(port, () => {
    console.log('***********************Game Server is running***********************');
});
let i = 0;
let canPlay = true ; 
let isLive=false;
let participants = [];
let watchers = [];
let nbAnswers = 0;
let io = socketIO(server).path('/game');
let comments = [];
//array contains index of correcte proposition for each question
let correcteAnswers = [1,2,0,0,1,2,1,2];
let localTrace = [];
let cagnotte = 20000;
let part = 0;
let current_session = null;
let socketEditeur = null;
let socketAnimateur = null;
let socketConsole = null;
let mode = 'm'; // 'a' pour auto ; m pour manuelle
let listGangnants = []; //this will contains list of username of winner
/** Hard coded example */
//#region hardcoded
let questions = [{content:"Comment se nomme l'équipe de football tunisienne ",
propositions:["Les Lions de Carthage","Les Aigles de Carthage","Les Rouges & Blancs"]},{
    content:'Par quelle mer la Tunisie est-elle bordée ',
    propositions : ["la mer rouge","morte","mediterranée"]
},  {
    content:'Quel est l hymne national de la Tunisie  ',
    propositions : ["Humat al hima","EL chaab","kassaman"]
},
{
    content:'Quelle est la langue officielle de la Tunisie  ',
    propositions : ["arabe","francais","italien"]
},
    {
    content:'combien de coupe d afrique possede la Tunisie Football',
    propositions : ["2","1","0"]
},
{
    content:'combien de coupe d afrique possede la Tunisie Handeball',
    propositions : ["8","9","10"]
},
{
    content:'combien de coupe d afrique possede la Tunisie basketball',
    propositions : ["0","1","2"]
},
{
    content:'en Tunisie 25 juillet est la fete',
    propositions:["mere","independance","republique"]
}]
//#endregion
let nb_question = questions.length;
//preparation des collections en Redis
initRedis();
//var interval = setInterval(function() {
  /*      
    if (participants.length >0){
        console.log('broadcasting');
        if(i === nb_question){console.log('clear interval');clearInterval(interval);}
        else{
            console.log(i);
            nbAnswers = 0 ; 
            var message = {id:'newQuestion',questionIndex:i,data:questions[i++]};
            io.sockets.emit("message",message);

        }
                }
}, 30000); */
io.on('connection', socket => {
    //console editeur
    socket.on('console',function(){

        socketConsole = socket ;
        socketConsole.emit('console',{id:'test',content:"test mesg"});
    });
    socket.on("animateurNext",function(message){
       if (socketAnimateur)socketAnimateur.emit("message",{id:"prepareYourSelf",index:message.index});
    });
    socket.on('nextQuestion',function(){
        if(interval)clearInterval(interval);
        mode = 'm';
        if (participants.length >-1){
                console.log(i);
                nbAnswers = 0 ; 
                var message = {id:'newQuestion',questionIndex:i,data:questions[i++],mode:mode};
                io.sockets.emit("message",message);
    
            }
            interval = setInterval(function() {
                mode = 'a';
                  if (participants.length >0){
                      console.log('broadcasting');
                      if(i === nb_question){console.log('clear interval');clearInterval(interval);}
                      else{
                          console.log(i);
                          nbAnswers = 0 ; 
                          var message = {id:'newQuestion',questionIndex:i,data:questions[i++],mode:mode};
                          io.sockets.emit("message",message);
              
                      }
                              }
              }, 60000);
      
            
    });
    //live or no
   socket.on("newSession",function(session){
        console.log("start signal");
        i = 0; 
        interval = setInterval(function() {
            mode = 'a';
              if (participants.length >0){
                  console.log('broadcasting');
                  if(i === nb_question){console.log('clear interval');clearInterval(interval);}
                  else{
                      console.log(i);
                      nbAnswers = 0 ; 
                      var message = {id:'newQuestion',questionIndex:i,data:questions[i++],mode:mode};
                      io.sockets.emit("message",message);
          
                  }
                          }
          }, 60000);
        current_session = session ;
        Session.findById({_id:session.idSession}).populate("questions").exec(function(err,populated){
            if(err)throw err;
            cagnotte = populated.cagnotte;
            initSession(populated.questions);
            if(socketEditeur){
                var nbwatcher = watchers.length + participants.length;
                socketEditeur.emit("message",{id:"quiz",quiz:questions,cagnotte:cagnotte,viewers:nbwatcher,step:i});
            }
       
            isLive = true ; 
            io.sockets.emit("isLiveResponse",{isLive:isLive});
        });
      
    });
    socket.on("animateurIn",function(){
        socketAnimateur = socket ;
        var nbwatcher = watchers.length + participants.length;
        socketAnimateur.emit("message",{id:"quiz",quiz:questions,cagnotte:cagnotte,viewers:nbwatcher,step:i});
    });
    socket.on("editeurIn",function(){
        socketEditeur = socket ;
        var nbwatcher = watchers.length + participants.length;
        socketEditeur.emit("message",{id:"quiz",quiz:questions,cagnotte:cagnotte,viewers:nbwatcher,step:i});
    });
    socket.on("isLive",function(){
        if(socketEditeur)socketEditeur.emit('message',{id:"isLive"});
        socket.emit("isLiveResponse",{isLive:isLive})
    });

     // error handle
    socket.on('error', error => {
        console.error(`Connection %s error : %s`, socket.id, error);
    });
 
    socket.on('disconnect', data => {
        participants   = participants.filter(function(item,i){
            if(item.id !== socket.id)
            return item;
        });
        watchers   = watchers.filter(function(item,i){
            if(item.id !== socket.id)
            return item;
        });
        var nbwatcher = watchers.length + participants.length;

        io.sockets.emit('message',{id:"update",watchers:nbwatcher});

    });

    socket.on('message', message => {
      switch (message.id) {
          case 'joinGame':joinGame(socket,message); if(socketConsole) socketConsole.emit("console",{id:"join",content:message.username});;break;
          case 'answer': processAnswer(socket,message);break;
          case 'newComment':processComment(socket,message); if(socketConsole) socketConsole.emit("console",{id:"comment",content:message.comment.username+":"+message.comment.content});break;
          default:socket.emit('invalide message');
      } 
    });
});
function joinGame (socket ,message ,callback){
    var nbwatcher = watchers.length + participants.length;
    nbwatcher++;
    if (canPlay) {
        //ajouter au liste des participants + emit state = participant
        socket.username = message.username;
        localTrace[message.username]=[];
        console.log(localTrace);
        participants.push(socket);
        var message = {id:'joinResponse',state:'participant',nbwatcher:nbwatcher}
        socket.emit('message',message);
    }else {
        //ajouter au liste des watchers + emit state = watcher
        watchers.push(socket);
        var message = {id:'joinResponse',state:'watcher',nbwatcher:nbwatcher}
        console.log(message);
        socket.emit('message',message);
    }
    io.sockets.emit('message',{id:"update",watchers:nbwatcher});
}
function processAnswer (socket,message) {
    console.log("************process answer ****************");
    nbAnswers++; //increment number of received answer
    console.log("received answers = "+nbAnswers);
    
    let username = message.username;
    let propositionIndex ;
    if(typeof message.propositionIndex === "undefined") {
        console.log("undefined");
        propositionIndex = 3;
    }else propositionIndex= message.propositionIndex;
    let questionIndex = message.questionIndex ;
    var reponse = questions[questionIndex].propositions[propositionIndex] ;
    console.log(username + " *** " + reponse);
    let questionPrefix = "traces:"+questionIndex;
    let redisKey = questionPrefix+":"+propositionIndex;
    
    localTrace[username].push(propositionIndex);
    addRedisTrace(redisKey,username);
  
    if(nbAnswers === participants.length){
        console.log("time to send response ");
        countAnswers(questionPrefix).then(function(result){
            console.log(result);
            var copie = participants ;
            var copieWatchers = watchers.slice(0);
            for(var l=0;l<participants.length;l++){
                var current = participants[l];
                var listResp = localTrace[current.username];
            
                if (listResp[questionIndex] === correcteAnswers[questionIndex]){
                    if(questionIndex === (nb_question-1)){
                        //le dernier question 
                        var itemResult = result[correcteAnswers[questionIndex]];
                        var nb_winner = itemResult.nb;
                        part = cagnotte / nb_winner ;
                        listGangnants.push(current.username);
                        current.emit("message",{id:"result",state:"participant",result:result,correcte:correcteAnswers[questionIndex],isLast:true,profit:part,step:i-1});

                    }else current.emit("message",{id:"result",state:"participant",result:result,correcte:correcteAnswers[questionIndex],isLast:false,step:i});
                }else {
                    //il faut le supprimer de la liste des participants et l'ajouter à la liste des watchers
                    copie   = copie.filter(function(item,i){
                        if(item.id !== current.id)
                        return item; 
                    
                    });
                    watchers.push(current);
                    current.emit("message",{id:"result",state:"watcher",result:result,correcte:correcteAnswers[questionIndex],isLast:false});
    
                }

            }
            if(socketEditeur)socketEditeur.emit("message",{id:"result",result:result,step:i,mode:mode});
            for(var k = 0 ;k<copieWatchers.length;k++)
             copieWatchers[k].emit("message",{id:"result",state:"watcher",result:result,correcte:correcteAnswers[questionIndex],isLast:false});
            participants = copie;
            if(questionIndex === nb_question - 1 || copie.length ===0){
              //  io.sockets.emit("message",{id:"closed"});
                isLive = false;
               // persiste();
                //send event closed to notifiy participants of the end of session + persist

            }

        }).catch(err => console.log(err));
       
    }
    //save to trace collection + increment number of answers + test if number of answers === number of participants
    //if answers = number of participants then send to each participants the result 
    
    //result message ={id:result,state: elemine ou nn,infos:[0.2,0.4,0.4]}
    if(i == questions.length){
        console.log('-------------------------end of session ---------------');
    }
}
async function countAnswers (prefix){
    var count = 0;
  var propositionOneKey = prefix + ":0";
  var propositionTwoKey = prefix + ":1";
  var propositionThreeKey = prefix + ":2";
  var elapsedKey = prefix + ":3";
  var nb_first_prop=  await redis.llen(propositionOneKey);
  var nb_seconde_prop=  await redis.llen(propositionTwoKey);
  var nb_third_prop=  await redis.llen(propositionThreeKey);
  var nb_last_prop=  await redis.llen(elapsedKey);
  var totale = nb_first_prop + nb_seconde_prop + nb_third_prop + nb_last_prop;
  var moy_first = nb_first_prop / totale;
  var moy_seconde = nb_seconde_prop / totale;
  var moy_third = nb_third_prop / totale;
  return [{moy:moy_first,nb:nb_first_prop},{moy:moy_seconde,nb:nb_seconde_prop},{moy:moy_third,nb:nb_third_prop}];
}
async function addRedisTrace(key,username){
  console.log("redisTrace");
  await  redis.rpush(key,username);
}
async function initRedis(){
    redis.flushall();
    for(var j=0;j<questions.length;j++){
    redis.rpush("traces",j);
 
        var name = "traces:"+j;
        redis.rpush(name,[0,1,2]);
    
   
    }
}
/**
 * processComment
 */

function processComment(socket,message){
    console.log("new comment ***************");
    var comment = message.comment ; // comment = {content: , username: , avatar_uri: }
    comments.push(comment);
    io.sockets.emit("message",{id:"comment",comment:comment});
}
/**
 * initSession
 */
function initSession(data){
  
    var quiz = data;
    questions = [];
    correcteAnswers=[];
    for(var i = 0 ; i<quiz.length ; i++){
        var current = quiz[i].toObject();
        var question = {
            content:current.content,
            propositions:[]
        };

      
        for(var j = 0 ;j<3;j++){
            if(current.propositions[j].estCorrecte)correcteAnswers.push(j);
            question.propositions.push(current.propositions[j].content);
        }
        questions.push(question);
    }
    nb_question = questions.length;

   
}
/**
 * persist : ==> this function persist data to mongodb after the end of session
 */
function persiste(){
    for (var key in localTrace){
        var currentInstance = new Participation();
        currentInstance.session_id = current_session.idSession;
        currentInstance.username = key ; 
        currentInstance.traces = localTrace[key].slice(0);
        if(listGangnants.indexOf(key)>=0){
            //winner
            currentInstance.eleminated = false ; 
            currentInstance.profit = part;
        }
        currentInstance.save(function(err,saved){
            if(err)throw err;
            console.log(saved);
        });
    };
}