var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var SessionSchema = new Schema({
date_dem:{ type: Date, default: Date.now ,required:true,min:Date.now,unique:true},
cagnotte:{  type: Number,default:2500,required:true},
commentaires:[{type:Schema.Types.ObjectId,ref:'Commentaire'}],
questions:[{type:Schema.Types.ObjectId,ref:'Question'}],
animateur:{type:Schema.Types.ObjectId,ref:'Admin'},
editeur:{type:Schema.Types.ObjectId,ref:'Admin'}
}
);
/**
 * 
 * @param {function} callback 
 */
SessionSchema.statics.nextSession = function(callback){
    this.aggregate([{$project:{date:"$date_dem",cagnotte:"$cagnotte",monthDay: 
    { $dateToString: { format: "%d/%m", date: "$date_dem" } },
temps:{$concat:[{ "$substr": [ { "$hour": "$date_dem" }, 0, 2 ] },":",
   { "$substr": [ { "$minute": "$date_dem" }, 0, 2 ] }]},encore:{$gt:["$date_dem",new Date()]}}}
   ,{$match:{"encore":true}},{$sort:{date:1}}],function(err,result){
        if(err)callback(err,null)
        else if (result.length>0)callback(null,result[0]);
        else callback(null,null);
    });
 }
 SessionSchema.statics.listSessions = function(callback){
    this.aggregate([
        {$project:{_id:"$_id",date:"$date_dem",cagnotte:"$cagnotte",etat:
                       {
                         $cond: { if: { $gte: [ "$date_dem", new Date() ] }, then: 1, else: 0 }
                       }
                   }},{$sort:{date:1}}
        ],function(err,result){
            if(err)callback(err,null)
        else callback(null,result);
        })
 }
SessionSchema.statics.deleteSession = function(id,callback) {
    let self = this;
    this.remove({_id:id},function(err,rm){
        if(err)callback(err,null);
        else callback(null,rm);
    })
 }
var Session=module.exports = mongoose.model('Session', SessionSchema);
