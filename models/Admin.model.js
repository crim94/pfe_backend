var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var AdminSchema = new Schema({
  nom: {type:String,default: "nom non defini"},
  prenom: {type:String,default: "prenom non defini"},
  username: {type:String,required: true, index: { unique: true }},
  pswd: { type: String, required: true },
  avatar:{type:String,default:"img/default.png"},
  role : { type:String,required: true}
});
AdminSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.pswd, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};
AdminSchema.pre('save', function(next) {
  var user = this;
   if (!user.isModified('pswd')) return next();
   bcrypt.genSalt(10, function(err, salt) {
       if (err) return next(err);
       bcrypt.hash(user.pswd, salt, function(err, hash) {
           if (err) return next(err);
           user.pswd = hash;
           next();
       });
});});
AdminSchema.statics.hashPassword = function(plain){
  var salt = bcrypt.genSaltSync(10);
  var hash = bcrypt.hashSync(plain, salt);
  return hash;
}
/**
 * this function returns the list of animators
 * @param {function} callback 
 */
AdminSchema.statics.getAnimateurs = function(callback){
    this.aggregate([{$match:{role:"animateur"}},{$project:{_id:0,value:"$_id",label:"$username"}}],function(err,docs){
        if (err) callback(err,null);
        else callback(null,docs);
    })
}
/**
 * this function returns the list of editors
 * @param {function} callback 
 */
AdminSchema.statics.getEditeurs = function(callback){
    this.aggregate([{$match:{role:"editeur"}},{$project:{_id:0,value:"$_id",label:"$username"}}],function(err,docs){
        if (err) callback(err,null);
        else callback(null,docs);
    })
}
var Admin=module.exports = mongoose.model('Admin', AdminSchema);
module.exports.verifyPassword=function(candidatePassword,hash){
return bcrypt.compareSync(candidatePassword, hash);
}
