var mongoose=require('mongoose');
var Schema = mongoose.Schema;
var CommentaireSchema = new Schema({
content:{ type: String,required:true},
date_com:{type:Date,required:true,default:Date.now},
auteur:{
  type:Schema.Types.ObjectId,required:true,ref:'Joueur'}
}
);
var Commentaire=module.exports = mongoose.model('Commentaire', CommentaireSchema);
