var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ParticipationSchema = new Schema({
session_id:{type:Schema.Types.ObjectId,ref:"Session",required:true},
username:{type:Schema.Types.String,required:true},
eleminated:{type:Schema.Types.Boolean,default:true},
profit:{type:Schema.Types.Number,default:0},
traces:[Number]
}
);
/**
 * update the player score
 */
ParticipationSchema.pre('save', function(next) {
    var participation = this;
    if(participation.eleminated === false){
        var username = participation.username;
        this.model("Joueur").findOneAndUpdate({username:username}, { $inc: { "profit_tot" : participation.profit } },function(err,updated){
            if(err)throw err;
            next(); 
        })
    }else next();
       
});
    
/**
 * 
 * @param {function} callback 
 * this function return the number of participants per session
 */
ParticipationSchema.statics.participantsPerSession = function(callback){
this.aggregate([{$group:{_id:"$session_id",nb_participants: {$sum: 1}
}},{$project:{_id:0,nb:"$nb_participants"}}],function(err,result){
if(err)callback(err,null)
callback(null,result);
});
}
/**
 * 
 * @param {function} callback 
 * this function return the maximum number of participants in all sessions
 */
ParticipationSchema.statics.maxParticipants = function(callback){

this.aggregate([{$group:{_id:"$session_id",nb_participants: {$sum: 1}
}},{$sort:{"nb_participants":-1}}],function(err,result){
if(err)callback(err,null)
else if (result.length>0)callback(null,result[0].nb_participants);
else callback(null,null);
});
//with mapreduce
/*var o = {},
self = this;
o.map = function () {
emit(this.session_id, 1)
};
o.reduce = function (k, vals) {
return vals.length
};
this.mapReduce(o,function(err,result){
    if (err)throw err;
    console.log(result);
})*/
}
/**
 * 
 * @param {function} callback 
 * this function returns the number of loser per session
 */
ParticipationSchema.statics.loserPerSession = function(callback){
this.aggregate([{$match:{"eleminated":true}},{$group:{_id:"$session_id",nb_participants: {$sum: 1}
        }}],function(err,result){   
                if(err)callback(err,null)
                callback(null,result);
                });
}
/**
 * 
 */
ParticipationSchema.statics.loserAndWinner = function(callback){
    this.model("Session").aggregate([{$lookup:{
        from:"loserPerSession",
        localField:"_id",
        foreignField:"_id",
        as:"losers"}},{$lookup:{
        from:"winnerPerSession",
        localField:"_id",
        foreignField:"_id",
        as:"winners"}},{$project:{date:"$date_dem","winners":{ $cond: [{$eq: [{$size: '$winners'}, 0] }, [ { nb_participants: 0 } 
        ], '$winners'] },"losers":{ $cond: [{$eq: [{$size: '$losers'}, 0] }, [ { nb_participants: 0 } 
        ], '$losers'] }}},{ "$unwind": "$winners" },{ "$unwind": "$losers" },
        {$project:{date:"$date",_id:"$_id",nb_loser:"$losers.nb_participants",nb_winner:"$winners.nb_participants"}}
,{$sort:{date:1}}],
function(err,result){
    if(err)callback(err,null)
       else callback(null,result);
})
}
/**
 * 
 * @param {function} callback 
 * this function returns the number of winner per session
 */
ParticipationSchema.statics.winnerPerSession = function(callback){
    this.aggregate([{$match:{"eleminated":false}},{$group:{_id:"$session_id",nb_participants: {$sum: 1}
        }}],function(err,result){   
                if(err)callback(err,null)
                callback(null,result);
                });

}
/**
 * classement par semaine
 */
ParticipationSchema.statics.classementParSemaine = function(callback){
   this.aggregate([{$match:{eleminated:false}},{$lookup:{
        from:"sessions",
        localField:"session_id",
        foreignField:"_id",
        as:"session"}},{$lookup:{
        from:"joueurs",
        localField:"username",
        foreignField:"username",
        as:"joueur"}},{$project:{username:"$username",
            avatar:"$joueur.avatar_uri",
        profit:"$profit",
        sessionDate:"$session.date_dem"}},{"$unwind":"$sessionDate"},{"$unwind":"$avatar"},
        {$project:{username:"$username",
        profit:"$profit",
        avatar_uri:"$avatar",
        currentWeek:{$week:[new Date()]},
        sessionWeek:{$week:["$sessionDate"]}}},{$project:{username:"$username",
        avatar_uri:"$avatar_uri",
        profit:"$profit",
      eq : {
        $cond : {
            if  : {
              //  $eq : ["$currentWeek", "$sessionWeek"]
              $eq : [23, "$sessionWeek"]
            },
        then : 1,
        else  : 0
    }}}},{$match:{eq:1}},{$group:{_id:"$username",username:{$first:"$username"},avatar_uri:{$first:"$avatar_uri"},profit:{"$sum":"$profit"}}},{$sort:{profit:-1}}
    ],function(err,result){
            if (err) callback(err,null);
            else callback(null,result);
        })
}
/**
 * 
 * @param {function} callback 
 * this function returns the number of elemination by question category
 */
ParticipationSchema.statics.loserPerCategory = function(callback){
    this.aggregate([
        {$match:{"eleminated":true}},
        {$lookup:{
            from:"populatedsession",
            localField:"session_id",
            foreignField:"_id",
            as:"sessionobj"}},
            { "$unwind": "$sessionobj" },
        {$project:
        {_id:"$_id",session:"$session_id",index:{$subtract:[{$size:"$traces"},1]},sessionobj:"$sessionobj"}}
        ,
        {$project:{_id:"$_id",session:"$session",categorie:{$arrayElemAt: [ "$sessionobj.questions.categorie", "$index" ]},
        }},
        {$group:{_id:"$categorie",value:{$sum:1}}},
        {$project:{name:"$_id",value:"$value",_id:0}}
    ],function(err,result){
        if (err) callback(err,null);
        else callback(null,result);
    });
}
var Participation=module.exports = mongoose.model('Participation', ParticipationSchema);
