var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var JoueurSchema = new Schema({
  numero:{type:String,required:true,index: { unique: true }},
  avatar_uri:{type:String,required:false,default:"avatars/default.png"},
  profit_tot:{type:Number,default:0},
  nb_jokers:{type:Number,default:0},
  username:{type:String ,required:false,default:"anonymous"},
  date_insc:{ type: Date, default: Date.now ,required:true},
});
/**
 * 
 * @param {function} callback 
 * this function returns the player who has the maximum score
 */
JoueurSchema.statics.bestPlayer = function(callback){
  this.findOne({}).sort({profit_tot:-1}).exec(function(err,max){
    if(err) callback(err,null);
    else callback(null,max);
  })
  }
  /**
   * 
   * @param {function} callback 
   * this function returns the classement of players
   */
JoueurSchema.statics.classementTotale = function(callback){
    this.find({}).sort({profit_tot:-1}).exec(function(err,joueurs){
      if(err) callback(err,null);
      else callback(null,joueurs);
    })
    }
    /**
     * 
     * @param {function} callback 
     * this function returns the number of inscription today
     */
JoueurSchema.statics.nouvelleInsc = function(callback){
  this.aggregate([
    {$project:{dateInsc: 
        { $dateToString: { format: "%Y-%m-%d", date: "$date_insc" } }
        ,now: 
        { $dateToString: { format: "%Y-%m-%d", date: new Date() } } }
        },{$project:{    date:"$dateInsc"  
    ,decision:{$cond: {
                if  : {
                    $eq : ["$dateInsc", "$now"]
                },
            then : 1,
            else  : 0
        }}}},{$match:{decision:1}}],function(err,result){
          if(err) callback(err,null);
      else callback(null,result);
        })
}
/**
 * 
 * @param {function} callback 
 * number of inscription per week
 */
JoueurSchema.statics.inscriptionPerWeek = function(callback){
  this.aggregate([
    {$project:{week: 
        { $week: ["$date_insc"]},year: 
        { $year: ["$date_insc"]}
    }},{$group:{_id:{year:"$year",week:"$week"},nbInsc:{$sum:1}}},{$project:{semaine:{$concat:[{ "$substr": ["$_id.week",
    0, 2 ] },"/",
   { "$substr": [ "$_id.year", 0, 4 ] }]},inscription:"$nbInsc"}},{$sort:{semaine:1}}],function(err,result){ 
       if(err) callback(err,null);
      else callback(null,result);})
}
JoueurSchema.statics.topFive = function(callback){
  this.find({}).sort({profit_tot:-1}).limit(5).exec(function(err,result){
    if(err) callback(err,null);
    else callback(null,result);})
}

var Joueur=module.exports = mongoose.model('Joueur', JoueurSchema);
