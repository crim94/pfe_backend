var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var QuestionSchema = new Schema ({
contenu: {type:String,required:true },
degre_dif:{ type:Number,required:true},
propositions: [{
  contenu: {type:String,required:true},
  estCorrecte:{type:Boolean,required:true,default:false}
}],
categorie:{type:String,required:true}
});
var Question=module.exports = mongoose.model('Question', QuestionSchema);
