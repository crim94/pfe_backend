var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var UserSchema = new Schema({
  nom: String,
  prenom:   String,
  date_naiss: { type: Date, default: Date.now },
  date_insc: { type: Date, default: Date.now },
  facebook:{
    id           : String,
     token        : String,
     name         : String,
     email        : String,
  },
  email: {type:String,required: false, index: { unique: true }},
  pswd: { type: String, required: false },
  avatar:String,
  sum_score:Number
});

/*****************************************************/
UserSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.pswd, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};
UserSchema.pre('save', function(next) {
  var user = this;

   // only hash the password if it has been modified (or is new)
   if (!user.isModified('pswd')) return next();
   // generate a salt

   bcrypt.genSalt(10, function(err, salt) {
       if (err) return next(err);

       // hash the password using our new salt
       bcrypt.hash(user.pswd, salt, function(err, hash) {
           if (err) return next(err);
           // override the cleartext password with the hashed one

           user.pswd = hash;
           next();
       });
});});

var User=module.exports = mongoose.model('User', UserSchema);
/**************************************Operations**************************************************/

module.exports.saveUser=function(instance,callback){
var user=new User(instance);
user.save(callback);
}
module.exports.verifyPassword=function(candidatePassword,hash){
return bcrypt.compareSync(candidatePassword, hash);

}
/**************************************************************************************************/
