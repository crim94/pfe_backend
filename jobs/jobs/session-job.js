
const MongoClient = require('mongodb').MongoClient;
var socket = require('socket.io-client')('http://localhost:4070');

module.exports = function(agenda) {
    agenda.define('session job', function(job, done) {

        // Connect to the db
        MongoClient.connect(process.env.MONGODB_URL, function(err, db) {

            if(!err) {
                console.log("session job");
                if(socket)socket.emit("newSession",{idSession:job.attrs.data.session_id});
                console.log("session started "+job.attrs.data.session_id);
                done();
            }
            if(err) {
               throw err;
            }
        });
    })
}