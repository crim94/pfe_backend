
const MongoClient = require('mongodb').MongoClient;
var admin = require("firebase-admin");
let GCLOUD_PROJECT='my-project-id';
var serviceAccount = require('../../utils/serviceAccount.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://pfe18-ed1a4.firebaseio.com"
});
var options = {
  priority: "high",
  timeToLive: 60 * 60 *24
};
var registrationToken = 'AAAAxj2oqvg:APA91bHoxGtY3AnlVwDq6JAy_2IBhUJ-4L-xJX3MDIiCA-NRVKU_USyt4qHvXSwV9MbLGgBPRSjfcdpSIUhly-7IeE5W4Idsum1EduYuCXRM226AbZ3z26_HrmpbxcPk01gkLimHYq7D';
var topic="session";


module.exports = function(agenda) {
    agenda.define('notification job', function(job, done) {

        // Connect to the db
        MongoClient.connect(process.env.MONGODB_URL, function(err, db) {
            if(!err) {
         
                let body ="une session va commencer apres 15 minutes,"+job.attrs.data.cagnotte+" à ganger aujourdhui" ;
                let title ="Chifco smart games" ;
                var notification = {
                    notification: {
                      body:body,
                      title: title
                    }
                  };
                  admin.messaging().sendToTopic(topic,notification)
                  .then((response) => {
                    // Response is a message ID string.
                    console.log('Successfully sent message:', response);
                  })
                  .catch((error) => {
                    console.log('Error sending message:', error);
                  });
                  done();
                
                }
            if(err) {
               throw err;
            }
        });
    })
}