db.getCollection('participations').
aggregate(
    [
        {$match:{"eleminated":true}},
        {$project:
        {_id:"$_id",session:"$session_id",index:{$subtract:[{$size:"$traces"},1]}}},
        {$lookup:{
            from:"populatedsession",
            localField:"session",
            foreignField:"_id",
            as:"sessionobj"}},
            { "$unwind": "$sessionobj" },

    ])
/** 
 * use of lookup 
 */
    db.getCollection('sessions').aggregate([{$lookup:{
        from:"questions",
        localField:"questions",
        foreignField:"_id",
        as:"questions"}}])
/**
 * create view
 */
//db.createView(<view>, <source>, <pipeline>, <options>)
db.createView(
    "populatedsession",
    "sessions",
    [{$lookup:{
        from:"questions",
        localField:"questions",
        foreignField:"_id",
        as:"questions"}}]
 )
 //
 db.createView(
    "loserPerSession",
    "participations",
    [{$match:{"eleminated":true}},{$group:{_id:"$session_id",nb_participants: {$sum: 1}
}}]
 )
 db.createView(
    "winnerPerSession",
    "participations",
    [{$match:{"eleminated":false}},{$group:{_id:"$session_id",nb_participants: {$sum: 1}
}}]
 )
 //drop a view : db.viewname.drop()
 //deux jointures avec winnerPerSession et loserPerSession
 db.sessions.aggregate([{$lookup:{
    from:"loserPerSession",
    localField:"_id",
    foreignField:"_id",
    as:"losers"}},{$lookup:{
    from:"winnerPerSession",
    localField:"_id",
    foreignField:"_id",
    as:"winners"}},            { "$unwind": "$winners" },            { "$unwind": "$losers" }

,{$project:{_id:"$_id",nb_loser:"$losers.nb_participants",nb_winner:"$winners.nb_participants"}}])
 /**
  * 
  */
 db.getCollection('participations').
aggregate(
    [
        {$match:{"eleminated":true}},
        {$lookup:{
            from:"populatedsession",
            localField:"session_id",
            foreignField:"_id",
            as:"sessionobj"}},
            { "$unwind": "$sessionobj" },
        {$project:
        {_id:"$_id",session:"$session_id",index:{$subtract:[{$size:"$traces"},1]},sessionobj:"$sessionobj"}}
        ,
        {$project:{_id:"$_id",session:"$session_id",questions:{$arrayElemAt: [ "$sessionobj.questions", "$index" ]}}}


    ])
    /**
     * number of elemination by categorie
     */
    db.getCollection('participations').
aggregate(
    [
        {$match:{"eleminated":true}},
        {$lookup:{
            from:"populatedsession",
            localField:"session_id",
            foreignField:"_id",
            as:"sessionobj"}},
            { "$unwind": "$sessionobj" },
        {$project:
        {_id:"$_id",session:"$session_id",index:{$subtract:[{$size:"$traces"},1]},sessionobj:"$sessionobj"}}
        ,
        {$project:{_id:"$_id",session:"$session",categorie:{$arrayElemAt: [ "$sessionobj.questions.categorie", "$index" ]},
        }},
        {$group:{_id:"$categorie",value:{$sum:1}}},
        {$project:{name:"$_id",value:"$value",_id:0}}
    ])
    /**
     * à completer : requete de Leaderboard
     */
    db.getCollection('participations').aggregate([{$match:{eleminated:false}},{$lookup:{
        from:"sessions",
        localField:"session_id",
        foreignField:"_id",
        as:"session"}},{$project:{username:"$username",
            profit:"$profit",
            sessionDate:"$session.date_dem"}},{"$unwind":"$sessionDate"},{$project:{username:"$username",
            profit:"$profit",
            currentWeek:{$week:[new Date()]},
            sessionWeek:{$week:["$sessionDate"]}}},{$project:{username:"$username",
            profit:"$profit",
          eq : {
            $cond : {
                if  : {
                    $eq : ["$currentWeek", "$sessionWeek"]
                },
            then : 1,
            else  : 0
        }}}},{$match:{eq:1}},{$group:{_id:"$username",profit:{"$sum":"$profit"}}},{$sort:{profit:-1}}
        ])
        /**
         * 
         */
        db.getCollection('sessions').aggregate([{$project:{date:"$date_dem",cagnotte:"$cagnotte",temps:
        {$concat:[{ "$substr": [ { "$hour": "$date_dem" }, 0, 2 ] },":",
    { "$substr": [ { "$minute": "$date_dem" }, 0, 2 ] }]},encore:{$gt:["$date_dem",new Date()]}}},{$match:{"encore":true}},{$sort:{date:1}}])