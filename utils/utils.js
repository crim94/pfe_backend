"use strict"
const jwt=require('jsonwebtoken');
exports.ensureToken=(req, res, next)=> {
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(" ");
    const bearerToken = bearer[1];
    req.token = bearerToken;
    try {
  var decoded = jwt.verify(req.token, process.env.SECRET);
    next()
    } catch(err) {
  res.sendStatus(403);
  }
  } else {
    res.sendStatus(403);
  }
}
