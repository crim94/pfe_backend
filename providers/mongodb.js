const mongoose = require('mongoose')
exports.check = () => {
    mongoose.Promise = global.Promise
    mongoose.set('debug', true)

    mongoose.connect(process.env.MONGODB_URL, null, (err) => {
        if (err) {
            console.log('---------------------------------------------------')
            console.error('------------- MongoDB is down: ' + err)
            console.log('---------------------------------------------------')
        } else {
            console.log('+++++++++++++++++++++++++++++++++++++++++++++++++++')
            console.log('+++++++++++++++ MongoDB is up +++++++++++++++++++++')
            console.log('+++++++++++++++++++++++++++++++++++++++++++++++++++')
        }
    })
}
