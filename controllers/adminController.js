const router = require('express').Router();
const bodyParser=require('body-parser');
const Admin=require("../models/Admin.model");
const jwt = require('jsonwebtoken');
const utils=require('../utils/utils');

router.get('/protected',utils.ensureToken,function(req,res,next){
  res.send('ok');
});
router.put("/updateRole",utils.ensureToken,function(req,res,next){
  Admin.findOneAndUpdate({username:req.body.user.username},{ $set: {role: req.body.user.role}},{new:true},function(err,docs){
    if(err) res.status(401);
    else res.status(200).json(docs);
  });
});
router.post("/upload",utils.ensureToken,function(req,res,next){
  var query = {username:req.body.username};
  let selectedFile = req.files.selectedFile;
  var filename = selectedFile.name;
  var splited = filename.split(".");
  var extension = splited[splited.length - 1];
  filename = req.body.username+"."+extension;
  var uri = '/img/'+filename;
  selectedFile.mv("public"+uri,function(err) {
   if (err)
     return res.status(500).send(err);
   else {
     Admin.findOneAndUpdate(query,{avatar:uri},{new:true},function(err,updated){
        if(!err)res.status(200).json(updated);
        else res.status(401).send(err);
     });
     }
});
});
router.post('/addAdmin',function(req,res,next){
var admin=new Admin(req.body.admin);

admin.save(function(err,instance){
  if(err) throw err;
  else res.status(200).json(instance);
});
});
router.post('/login',function(req,res,next){
Admin.findOne({ 'username' : req.body.username }, function(err, user) {
  console.log(user);
if(!err){
  if(user){
            if(Admin.verifyPassword(req.body.password,user.pswd))  {
              var token = jwt.sign({ username: user.username , role: user.role,id:user._id}, process.env.SECRET);
              res.status(200).json({user:user,token:token});}
            else res.status(404).json({content:"verifiez votre mots de passe"});
          }
  else res.status(404).json({content:"verifiez svp"});
}
else res.status(404).json({content:"verifiez svp"});
});
});
router.put("/updateAdmin",utils.ensureToken,function(req,res,next){
  var hash = Admin.hashPassword(req.body.admin.pswd);
  var admin = Object.assign({},req.body.admin);
  admin.pswd = hash ;
  Admin.findOneAndUpdate({_id:req.body.id},admin,{new:true},function(err,user){
    if(err)res.status(401).json({message:"verifiez svp "})
    else {
      console.log(user);
      res.status(200).json({user:user});}
  });
});
router.get("/all",utils.ensureToken,function(req,res,next){

  Admin.find({username : {$ne: req.query.current}}).exec(function(err,docs){
    if(err) res.status(401);
    else res.status(200).json(docs);
  });
});
router.get("/animateurs",utils.ensureToken,function(req,res,next){
  Admin.getAnimateurs(function(err,docs){
    if(err){throw err;res.status(500)}
    else {console.log(docs);res.status(200).json(docs);}
  });
})
router.get("/editeurs",utils.ensureToken,function(req,res,next){
  Admin.getEditeurs(function(err,docs){
    if(err){throw err;res.status(500)}
    else {console.log(docs);res.status(200).json(docs);}
  });
})
module.exports = router
