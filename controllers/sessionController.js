const router = require('express').Router();
const bodyParser=require('body-parser');
const Session=require("../models/Session.model");
const jwt = require('jsonwebtoken');
const utils=require('../utils/utils');
let agenda = require('../jobs/agenda');
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
// Connect to the db
let connection=null;

MongoClient.connect(process.env.MONGODB_URL, function(err, client) {
  if(!err) {
    console.log("we are connected");
    connection = client.db();
 
   
  }else throw err;});

router.post("/addSession",utils.ensureToken,function(req,res,next){
  var newsession = new Session(req.body.session);
  newsession.save(function(err,session){
    if(!err){res.status(200).json(session);
    //schedulle a job with data and time of this session = 

    agenda.schedule(session.date_dem, 'session job', {session_id: session._id});
    var date = new Date(session.date_dem);
    date.setMinutes(date.getMinutes() - 15);
    console.log(session);
    agenda.schedule(date,'notification job',{session_id:session._id,cagnotte:session.cagnotte})
 

     }
    else res.status(401).json({message:"l ajout de session a echoue"});
  });
});
router.get("/getAll",utils.ensureToken,function(req,res,next){
  Session.find().populate("questions").exec(function(err,sessions){
    if(!err)console.log(sessions[0].questions[0]);
    else console.log("erreur");
  });
});
router.get("/nextSession",utils.ensureToken,function(req,res,next){
Session.nextSession(function(err,session){
  if(err){res.status(500);throw err;}
  else res.status(200).json(session);
})
});
router.get("/listSession",utils.ensureToken,function(req,res,next){
  Session.listSessions(function(err,sessions){
    if(err){res.status(500);throw err;}
    else res.status(200).json(sessions);
  })
  });
router.delete("/delete",utils.ensureToken,function(req,res,next){
  Session.deleteSession(req.query._id,function(err,removed){
    if (err){res.status(500);throw err ; }
    else {
      connection.collection("jobs").remove({"data.session_id":req.query._id}, function(er, result) {
        if (er) {res.status(500);throw er};
        connection.collection("jobs").remove({"data.session_id":ObjectID(req.query._id)},function(err,docs){
          if (er) {res.status(500);throw er;}
          res.status(200).json({removed:true});
        })
      });   
    }
  })
})
module.exports = router
