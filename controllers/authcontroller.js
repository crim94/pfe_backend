const router = require('express').Router()
const passport = require('passport');
const User=require('../models/User.model');
const utils=require('../utils/utils');
router.get('/success',function(req,res){
/*  jwt.verify(req.token, process.env.SECRET, function(err, data) {
    if (err) {
      res.sendStatus(403);
    } else {
      res.json({
        description: 'Protected information. Congrats!'
      });
    }
  });*/
  console.log('success');
});
router.get('/hello',function(req,res,next){
  res.send(JSON.stringify({content:'votre service fonctionne correctememnt'}));
})
router.get('/login',function(req,res,next){
  res.send(JSON.stringify({content:'none'}));

});
router.get('/facebook', passport.authenticate('facebook', {
     scope : ['public_profile', 'email']
   }));

   // handle the callback after facebook has authenticated the user
router.get('/facebook/callback',
       passport.authenticate('facebook', {
           failureRedirect : '/auth/login'
       }),function(req,res){
         console.log('nv success');
         console.log(req.user);

         res.redirect('/auth/success');
       });
router.post('/local', passport.authenticate('local', { failureRedirect: '/auth/login' }),
       function(req, res) {
         res.send(JSON.stringify({content:'ok'}));
});

router.post('/inscription',function(req,res,next){
User.saveUser(req.body,function(err,saved){
if(!err) {
  res.send(JSON.stringify({code:1,content:'Vous etes inscrit,bienvenu '+saved.nom}));
}
else res.send(JSON.stringify({code:0,content:'Essayez de nouveau'}));
});
});
router.post('/facebook/token',
  passport.authenticate('facebook-token'),
  function (req, res) {
    res.send(req.user? 200 : 401);
  }
);
module.exports = router
