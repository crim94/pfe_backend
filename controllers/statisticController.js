const router = require('express').Router();
const bodyParser=require('body-parser');
const jwt = require('jsonwebtoken');
const utils=require('../utils/utils');
const Participation =  require("../models/Participation.model");
const Joueur =  require("../models/Joueur.model");

router.get('/max',utils.ensureToken,function(req,res,next){
    Participation.maxParticipants(function(err,result){
        if(err){res.status(501);throw err;}
        res.status(200).json(result);
    });
  });
router.get("/parcategorie",utils.ensureToken,function(req,res,next){
    Participation.loserPerCategory(function(err,result){
        if(err){res.status(500);throw err;}
        else res.status(200).json(result);
    });
});
router.get("/participantParSession",utils.ensureToken,function(req,res,next){
    Participation.participantsPerSession(function(err,result){
        if(err){res.status(500);throw err;}
        else res.status(200).json(result);
    });
})
router.get("/winnerAndLoser",utils.ensureToken,function(req,res,next){
    Participation.loserAndWinner(function(err,result){
        if(err){res.status(500);throw err;}
        else res.status(200).json(result);
    });
});
router.get("/bestPlayer",utils.ensureToken,function(req,res,next){
    Joueur.bestPlayer(function(err,player){
        if (err) res.status(500);
        else res.status(200).json(player);
    });
});
router.get("/classementTot",utils.ensureToken,function(req,res,next){
Joueur.classementTotale(function(err,joueurs){
    if(err){res.status(500);throw err;}
    else res.status(200).json(joueurs);
})
});
router.get("/classementParSemaine",utils.ensureToken,function(req,res,next){
    Participation.classementParSemaine(function(err,joueurs){
        if(err){res.status(500);throw err;}
        else res.status(200).json(joueurs);
    })
    });
router.get("/nbInscription",utils.ensureToken,function(req,res,next){
Joueur.nouvelleInsc(function(err,inscriptions){
    if(err){res.status(500);throw err;}
    else res.status(200).json({nb:inscriptions.length});
});
});
router.get("/inscriptionPerWeek",utils.ensureToken,function(req,res,next){
    Joueur.inscriptionPerWeek(function(err,inscriptions){
        if(err){res.status(500);throw err;}
        else res.status(200).json(inscriptions);
    });
})
router.get("/topFive",utils.ensureToken,function(req,res,next){
    Joueur.topFive(function(err,result){
        if(err){res.status(500);throw err;}
        else res.status(200).json(result);
    })
});
module.exports = router
