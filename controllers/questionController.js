const router = require('express').Router();
const bodyParser=require('body-parser');
const Question=require("../models/Question.model");
const jwt = require('jsonwebtoken');
const utils=require('../utils/utils');
router.post("/saveAll",utils.ensureToken,function(req,res,next){
  Question.collection.insert(req.body.questions, function (err, docs) {
       if (err){
         res.status(401).json({message:"operation d insertion a echoue"});
       } else {
         res.status(200).json(docs);
       }
     });
   });
router.get("/getAll",utils.ensureToken,function(req,res,next){
  Question.find().select({content:1,categorie:1}).exec(function(err,questions){
    if(!err)res.json(questions);
    else res.status(401);
  })
});
module.exports = router;
