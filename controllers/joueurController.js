const router = require('express').Router();
const bodyParser=require('body-parser');
const Joueur=require("../models/Joueur.model");
const jwt = require('jsonwebtoken');
const utils=require('../utils/utils');


router.post("/subscribe",function(req,res,next){
  let numero = req.body.numero;
  Joueur.findOne({numero:numero},function(err,instance){
    if(err) throw err;
    else {
    if(!instance){
        new Joueur({numero:numero}).save(function(err,saved){
          if(err)throw err;
          else {var token = jwt.sign({ numero: numero}, process.env.SECRET);
                res.status(200).json({token:token,joueur:saved,new:1});
              }
    });

    }else {
      var token = jwt.sign({ numero: numero}, process.env.SECRET);
                res.status(200).json({token:token,joueur:instance,new:0});
    }
  }
});
});
router.post("/updateInfo",utils.ensureToken,function(req,res,next){
let selectedFile = req.files.avatar;
  var filename = selectedFile.name;
  var splited = filename.split(".");
  var extension = splited[splited.length - 1];
  filename = req.body.username+"."+extension;
  var uri = '/avatars/'+filename;
  console.log(filename);
  selectedFile.mv("public"+uri,function(err) {
  if(err)throw err;
    else  Joueur.findOneAndUpdate({numero:req.body.numero},{avatar_uri:uri,username:req.body.username},{new:true},function(err,updated){
      if(!err)res.status(200).json(updated);
      else res.status(401).send(err);
   })
});
});  
router.get("/:numero",utils.ensureToken,function(req,res,next){
let numero =req.params.numero;
Joueur.findOne({numero:numero},function(err,instance){
  if (err) throw err;
  else res.status(200).json(instance);
});
});
module.exports = router
