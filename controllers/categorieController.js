const router = require('express').Router();
const bodyParser=require('body-parser');
const Categorie=require("../models/Categorie.model");
const jwt = require('jsonwebtoken');
const utils=require('../utils/utils');
const categories =[
  {
    value: 'tech',
    label: 'tech',
  },
   {
     value: 'culture',
     label: 'culture',
   },
   {
    value: 'sport',
    label: 'sport',
  },
   {
     value: 'science',
     label: 'science',
   },
   {
     value: 'publicite',
     label: 'publicite',
   },
   { value: 'islamic',
    label: 'islamic'},
   {
    value: 'histoire',
    label: 'histoire'
   }
   
 ];
router.get('/getAll',utils.ensureToken,function(req,res,next){
  res.json(categories);
})
module.exports = router;
